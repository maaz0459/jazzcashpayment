import React,{useState} from 'react'
import {Row,Col,Button, Input,Alert,Spin} from 'antd'
import axios from 'axios'
import './App.css'
import 'antd/dist/antd.css';
function App() {
  const [amount,setAmount]=useState("")
  const [cardNumber,setCardNumber]=useState("")
  const [successCode,setSuccessCode]=useState("")
  const [spin,setSpin]=useState(false)

  const payment= ()=>{
    setSuccessCode("")
    setSpin(true)
     axios({
      method: 'post',
      url: 'http://localhost:3001/jazzcash',
      data: {
          amount: amount,
          cardNumber:cardNumber
      }
  }).then(res => {
     if(res.data.responseCode==="000"){
       setSpin(false)
       setSuccessCode("OK")

     }
  }).catch(err => {
    setSpin(false)
      setSuccessCode("Err")

  });
    
  }
  return (
    <>
    
    <div style={{display:"flex",justifyContent:"center",marginTop:"5%"}} >
      
      <div className="mainBlock" style={{height:"500px",boxShadow:"0px 1px 3px 0px rgba(0,0,0,0.71)",textAlign:"center"}}> 
      <img  style={{maxWidth:"50%"}} src="https://www.pngkit.com/png/full/291-2911793_jazz-cash-jazz-logo.png" alt=""/>
     <div>
     {spin&&<Spin style={{marginTop:"10%"}} size="large" />}

     </div>
     {successCode==="OK" &&<Alert
      message="Success Tips"
      description="Detailed description and advice about successful copywriting."
      type="success"
      showIcon
      style={{marginTop:"5%"}}
    />}
    {successCode==="Err"&&<Alert
      message="Error"
      description="Payment Unsuccessfull."
      type="error"
      showIcon
      style={{marginTop:"5%"}}
    />}
      <div style={{justifyContent:"center",display:"flex",marginTop:successCode===""?spin?"10%":"20%":"5%"}}>
      <Row style={{width:"70%"}}>
        <label htmlFor="cardNumber">Amount</label>
        <Input
        value={amount}
        onChange={(e)=>setAmount(e.target.value)}
        />
<label htmlFor="cardNumber">Card Number</label>
        <Input
        value={cardNumber}

        onChange={(e)=>setCardNumber(e.target.value)}
        
        />
<Input.Group style={{marginTop:"5%"}} size="large">
      <Row gutter={12}>
        <Col span={5}>
        <label htmlFor="expiry">Month</label>
          <Input defaultValue="0571" />
        </Col>
        <Col span={5}>
        <label htmlFor="expiry">Year</label>
          <Input defaultValue="0571" />
        </Col>
        <Col span={14}>
        <label htmlFor="cvv">CVV</label>
          <Input defaultValue="266" />
        </Col>
      </Row>
    </Input.Group>
    <Button onClick={payment} style={{width:"100%",marginTop:"10%",borderRadius:"5px"}} type="primary" title="Payment">Payment</Button>
      </Row>
      
      </div>
    </div></div></>
  );
}

export default App;
