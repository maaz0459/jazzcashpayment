var express = require("express");
var cors=require("cors")
var bodyParser = require('body-parser');
var jazzcash=require("./routes")


var app=express()
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors())
app.use('/',jazzcash)
app.listen(3001,()=>{
    console.log("Server running on port 3001")
})